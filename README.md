#Project FrozenFist
##VortexOps

![This icon is provided by icons8 as Creative Commons Attribution-NoDerivs 3.0](https://www.iconsdb.com/icons/preview/icon-sets/ice/clenched-fist-xxl.png)

The team is currently working on a project codenamed "Frozen Fist." They are designing software for semi-autonomous armored assault vehicles. The vehicles are designed to operate either remotely with a human pilot or in "robot mode" with guidance from an on-board Artificial Intelligence (AI) system.

Primary Contact: [Justin Nyland](justin.nyland@smail.rasmussen.edu)

###Pushed to BitBucket repository - 05/16/2018
### Modified README on BitBucket - 05/23/2018